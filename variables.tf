variable "snapshot_id" {
  type = string
}

variable "name" {
  type = string
}

variable "ssh_keys" {
  type = list(string)
}

variable "location" {
  type    = string
  default = "nbg1"
}

variable "server_type" {
  type    = string
  default = "cpx31"
}

variable "labels" {
  type = map(string)
}

variable "placement_group_id" {
  type    = string
  default = null
}
