terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = ">=1.24.1"
    }
  }
}

resource "hcloud_server" "nixos_instance" {
  name               = var.name
  image              = var.snapshot_id
  server_type        = var.server_type
  ssh_keys           = var.ssh_keys
  keep_disk          = true
  location           = var.location
  placement_group_id = var.placement_group_id

  labels = var.labels

  lifecycle {
    ignore_changes = [
      ssh_keys
    ]
  }
}

resource "null_resource" "resize_partition" {
  triggers = {
    public_ip = hcloud_server.nixos_instance.ipv4_address
  }

  connection {
    type  = "ssh"
    host  = hcloud_server.nixos_instance.ipv4_address
    agent = true
  }

  provisioner "remote-exec" {
    script = "${path.module}/resizeInstanceFs.sh"
  }
}
