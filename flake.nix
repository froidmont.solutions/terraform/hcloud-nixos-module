{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs }:

    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;

      inputs = with pkgs; [
        terraform
        terraform-ls
      ];
    in
      {
        devShell.x86_64-linux = pkgs.mkShell {
          buildInputs = inputs;
        };
      };
}
