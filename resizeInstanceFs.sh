 #!/usr/bin/env bash
 
first_sector=$(cat /sys/block/sda/sda1/start)

(
echo d # Delete partition
echo 1
echo n # Add a new partition
echo 1 # Partition number
echo $first_sector  # First sector
echo   # Last sector (Accept default: max size)
echo w # Write changes
) | sudo fdisk /dev/sda

partx /dev/sda # Load new partition table
resize2fs /dev/sda1 # Resize the filesystem