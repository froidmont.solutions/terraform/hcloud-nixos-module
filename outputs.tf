output "public_ip" {
  value = hcloud_server.nixos_instance.ipv4_address
}

output "id" {
  value = hcloud_server.nixos_instance.id
}
